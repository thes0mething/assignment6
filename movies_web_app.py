import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE moviesdb(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT UNSIGNED NOT NULL, title VARCHAR(100) NOT NULL, director VARCHAR(100) NOT NULL, actor VARCHAR(100) NOT NULL, release_date VARCHAR(100) NOT NULL, rating DECIMAL(3,1) NOT NULL, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    cur.execute("SELECT title FROM moviesdb")
    entries = [dict(title=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_to_db():
    print("Received request.")
    output = ''
    exists = 0

    # Grab all the information
    # Safe to assume all input is always entered so no 'try' needed
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    # check if movie with title already exists
    try:
        cur.execute("SELECT EXISTS (SELECT 1 FROM moviesdb WHERE title='" + title + "') AS smovie;")
        exists = cur.fetchone()[0]
    except Exception as exp:
        print('error')
        output = 'Movie ' + title + ' could not be inserted - ' + exp.message


    cur = cnx.cursor()
    if exists == 0:
        try:
            cur.execute("INSERT INTO moviesdb (year, title, director, actor, release_date, rating) values (" + year + ", '" + title + "', '" + director + "', '" + actor + "', '" + release_date + "', "+ rating + ")")
            cnx.commit()
            output = 'Movie ' + title + ' successfully inserted'
        except Exception as exp:
            output = 'Movie ' + title + ' could not be inserted - ' + exp.message
    else:
        output = 'Movie with title ' + title + ' already exists'

    return render_template('index.html', message=output)

@app.route('/delete_movie', methods=['POST'])
def delete_from_db():
    print("Received request.")
    output = ''
    change = 0

    dtitle = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("DELETE FROM moviesdb WHERE title='" + dtitle + "';")
        change = cur.rowcount
        cnx.commit()
        # if a row changed we deleted something, otherwise it doesn't exist
        if change == 1:
            output = 'Movie ' + dtitle + ' successfully deleted'
        else:
            output = 'Movie with ' + dtitle + ' does not exist'
    except Exception as exp:
        output = 'Movie ' + dtitle + ' could not be deleted - ' + exp.message

    return render_template('index.html', message=output)

@app.route('/update_movie', methods=['POST'])
def update_db():
    print("Received request.")
    ouput = ''
    change = 0

    # Grab all the information
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("UPDATE moviesdb SET year=" + year + ", director='" + director + "', actor='" + actor + "', release_date='" + release_date + "', rating=" + rating + " WHERE title= '" + title + "'")
        change = cur.rowcount
        print(change)
        cnx.commit()
        # if a row changed, then we updated something, otherwise it doesn't exist
        if change == 1:
            output = 'Movie ' + title + ' successfully updated'
        else:
            output = 'Movie with ' + title + ' does not exist'
    except Exception as exp:
        output = 'Movie ' + title + ' could not be updated - ' + exp.message

    return render_template('index.html', message=output)

@app.route('/search_movie', methods=['GET'])
def search_db():
    print("Received request.")
    output = ''
    found = 0
    # Grab all the information
    actor = request.args.get('search_actor')
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT year, title, actor FROM moviesdb WHERE actor='" + actor + "';")
        for (year, title, actor) in cur:
            found = 1
            output += ("{}, {}, {} \n ".format(title, year, actor))
    except Exception as exp:
        print(exp)

    if found == 0:
        output = 'No movies found for actor ' + actor

    return render_template('index.html', message=output)

@app.route('/highest_rating', methods=['GET'])
def hr_db():
    print("Received request.")
    output = ''

    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    try:
        cur = cnx.cursor()
        cur.execute("SELECT year, title, director, actor, rating FROM moviesdb WHERE rating = (SELECT max(rating) FROM moviesdb)")
        for (year, title, director, actor, rating) in cur:
            output += ("{}, {}, {}, {}, {} \n ".format(title, year, actor, director, rating))
    except Exception as exp:
        print(exp)
    return render_template('index.html', message=output)

@app.route('/lowest_rating', methods=['GET'])
def lr_db():
    print("Received request.")
    output = ''
 

    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT year, title, director, actor, rating FROM moviesdb WHERE rating = (SELECT min(rating) FROM moviesdb)")

        for (year, title, director, actor, rating) in cur:
            output += ("{}, {}, {}, {}, {} \n ".format(title, year, actor, director, rating))
    except Exception as exp:
        print(exp)
    return render_template('index.html', message=output)

@app.route("/")
def hello():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', message='')


if __name__ == "__main__":
    app.debug = False
    app.run(host='0.0.0.0')
